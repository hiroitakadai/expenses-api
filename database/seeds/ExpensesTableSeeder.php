<?php

use Illuminate\Database\Seeder;
use App\ExpenseSummary;

class ExpensesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('expenses')->insert([
            ['user_id' => 1, 'accounted_at' => '2019/12/01', 'income' => 20000, 'outcome' => 0, 'note' => 'note1', 'title_id' => '15'],
            ['user_id' => 1, 'accounted_at' => '2019/12/01', 'income' => 0, 'outcome' => 1000, 'note' => 'note2', 'title_id' => '1'],
            ['user_id' => 1, 'accounted_at' => '2019/12/01', 'income' => 0, 'outcome' => 5000, 'note' => 'note3', 'title_id' => '2'],
            ['user_id' => 1, 'accounted_at' => '2019/12/01', 'income' => 0, 'outcome' => 300, 'note' => 'note4', 'title_id' => '3'],
            ['user_id' => 1, 'accounted_at' => '2019/12/02', 'income' => 0, 'outcome' => 6000, 'note' => 'note5', 'title_id' => '4'],
            ['user_id' => 1, 'accounted_at' => '2019/12/03', 'income' => 0, 'outcome' => 3000, 'note' => 'note6', 'title_id' => '5'],
            ['user_id' => 1, 'accounted_at' => '2019/12/05', 'income' => 0, 'outcome' => 500, 'note' => 'note7', 'title_id' => '6'],
            ['user_id' => 1, 'accounted_at' => '2019/12/05', 'income' => 0, 'outcome' => 20, 'note' => 'note8', 'title_id' => '7'],
            ['user_id' => 1, 'accounted_at' => '2019/12/05', 'income' => 0, 'outcome' => 10000, 'note' => 'note9', 'title_id' => '8'],
            ['user_id' => 1, 'accounted_at' => '2019/12/05', 'income' => 0, 'outcome' => 4000, 'note' => 'note10', 'title_id' => '9'],
            ['user_id' => 1, 'accounted_at' => '2019/12/05', 'income' => 0, 'outcome' => 2000, 'note' => 'note11', 'title_id' => '10'],
            ['user_id' => 1, 'accounted_at' => '2019/12/10', 'income' => 0, 'outcome' => 500, 'note' => 'note12', 'title_id' => '11'],
            ['user_id' => 1, 'accounted_at' => '2019/12/10', 'income' => 0, 'outcome' => 2000, 'note' => 'note13', 'title_id' => '12'],
            ['user_id' => 1, 'accounted_at' => '2019/11/01', 'income' => 100000, 'outcome' => 0, 'note' => 'note14', 'title_id' => '16'],
            ['user_id' => 1, 'accounted_at' => '2019/11/01', 'income' => 0, 'outcome' => 30, 'note' => 'note14', 'title_id' => '13'],
            ['user_id' => 1, 'accounted_at' => '2019/11/01', 'income' => 0, 'outcome' => 7000, 'note' => 'note15', 'title_id' => '14'],
            ['user_id' => 1, 'accounted_at' => '2019/11/01', 'income' => 0, 'outcome' => 8000, 'note' => 'note16', 'title_id' => '15'],
            ['user_id' => 2, 'accounted_at' => '2019/11/01', 'income' => 0, 'outcome' => 1000, 'note' => 'note17', 'title_id' => '20'],
        ]);

        ExpenseSummary::aggregate('2019/11/01', 1);
        ExpenseSummary::aggregate('2019/12/01', 1);
        ExpenseSummary::aggregate('2019/11/01', 2);
    }
}
