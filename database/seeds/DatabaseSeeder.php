<?php

use App\Title;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            ['name' => "name1", 'email' => "test1@test.test", "password" => Hash::make("password"), "budget" => 200000]
        );

        DB::table('users')->insert(
            ['name' => "name2", 'email' => "test2@test.test", "password" => Hash::make("password"), "budget" => 200000]
        );

        DB::table('users')->insert(
            ['name' => "name3", 'email' => "test3@test.test", "password" => Hash::make("password"), "budget" => 200000]
        );

        Title::initData(1);

        Title::initData(2);

        Title::initData(3);

        $this->call(ExpensesTableSeeder::class);
    }
}
