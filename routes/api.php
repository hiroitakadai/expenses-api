<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('user', 'UserController@store');

Route::post('login', 'UserController@login');

Route::middleware('auth:api')->group(function () {
    Route::get('user', 'UserController@show');
    Route::patch('user', 'UserController@update');
    Route::delete('user', 'UserController@destroy');

    Route::apiresource('expenses', 'ExpenseController')->except(['show']);
    Route::get('expenses/period', 'ExpenseController@period');
    Route::get('expenses/summary', 'ExpenseController@summary');

    Route::apiresource('titles', 'TitleController')->except(['show']);;

    Route::post('logout', 'UserController@logout');
});
