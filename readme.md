# API

## ユーザー

### POST /login

ログイン

Request Body

```json
{
  "email": "test@test.test",
  "password": "password"
}
```

Response Body

```json
{
  "data": {
    "token": "UYMEiWL5CuWXCOkSZyMXO4vKdJ3WfIVWXNmasESl95DLneRdUWktOfjWXyUp"
  }
}
```

### DELETE /logout

ログアウト

Response Body

```json
{}
```

### POST /user

ユーザー登録

Request Body

```json
{
  "name": "name",
  "email": "test@test.test",
  "password": "password",
  "password_confirmation": "password",
  "budget": "20000"
}
```

Response Body

```json
{
  "data": {
    "token": "UYMEiWL5CuWXCOkSZyMXO4vKdJ3WfIVWXNmasESl95DLneRdUWktOfjWXyUp"
  }
}
```

### GET /user

ユーザー情報取得

Response Body

```json
{
  "data": {
    "name": "name",
    "email": "test@test.test",
    "budget": "20000"
  }
}
```

### PATCH /user

### PUT /user

ユーザー情報修正
パスワードを変更した場合のみレスポンスでトークンを返す

Request Body

```json
{
  "name": "name",
  "email": "test@test.test",
  "password": "password",
  "password_confirmation": "password",
  "budget": "50000"
}
```

Response Body

```json
{
  "data": {
    "token": "UYMEiWL5CuWXCOkSZyMXO4vKdJ3WfIVWXNmasESl95DLneRdUWktOfjWXyUp"
  }
}
```

### DELETE /user

ユーザー削除

Response Body

```json
{}
```

## 取引

### GET /expenses

取引一覧を取得

Parameter

```html
  ?period=2019-12
```

Response Body

```json
{
  "data": [
    {
      "id": 1,
      "accounted_at": "2019-12-01",
      "title_id": 15,
      "title_name": "\u7d66\u4e0e",
      "income": 20000,
      "outcome": 0,
      "note": "note"
    },
    ...
  ],
  "meta": {
    "transfer": 84970,
    "income": 20000,
    "outcome": 34320
  }
}
```

### POST /expenses

取引を登録

Request Body

```json
{
  "accounted_at": "2019-12-02",
  "title_id": 4,
  "amount": "6000",
  "note": "note5",
  "type": "outcome"
}
```

Response Body

```json
{
  "data": {
  "id": 5
  }
}
```

### PATCH /expenses/{expense}

### PUT /expenses/{expense}

取引を修正

Request Body

```json
{
  "accounted_at": "2019-12-02",
  "title_id": 4,
  "amount": "6000",
  "note": "note5",
  "type": "outcome"
}
```

Response Body

```json
{
  "data": {
    "income": 20000,
    "outcome": 34320
  }
}
```

### DELETE /expenses/{expense}

取引を削除

Response Body

```json
{
  "data": {
    "income": 20000,
    "outcome": 34320
  }
}
```

### GET /expenses/period

取引がある年月の一覧を取得

Response Body

```json
{
  "data":[
  {
    "period": "2019-12",
    "count": 13
  },
  ...
  ]
}
```

### GET /expenses/summary

当月のまとめを取得

Response Body

```json
{
  "data": [
    {
      "accounted_at": "2019-12-01",
      "outcome": 6300,
      "balance": 193700
    },
    ...
  ],
  "meta": {
    "budget": 200000,
    "income": 20000,
    "outcome": 33820
  }
}
```

## 費目

### GET /titles

費目一覧を取得

Response Body

```json
{
  "data": [
    {
      "value": 1,
      "text": "\u98df\u8cbb"
    },
    ...
  ]
}
```

### POST /titles

費目を追加

Request Body

```json
{
  "name": "その他",
  "type": "income"
}
```

Response Body

```json
{
  "data": { "id": 75 }
}
```

### PATCH /titles/{title}

### PUT /titles/{title}

費目を修正

Request Body

```json
{
  "name": "その他"
}
```

Response Body

```json
{}
```

### DELETE /titles/{title}

費目を削除

Response Body

```json
{}
```
