<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    protected $url = '/api/user';

    private static $token4 = null;

    private static $token5 = null;

    /**
     * @dataProvider provideData
     * @dataProvider provideDataCreate
     */
    public function testCreateFail($data, $expect)
    {
        $response = $this->json('POST', $this->url, $data);
        $response->assertStatus(422)
            ->assertJson($expect);
        $this->assertDatabaseMissing('users', ['id' => 4]);
    }

    public function testCreateSuccess()
    {
        $data = [
            'name' => 'あ',
            'email' => 'test10@test.test',
            'password' => 'pppppp',
            'password_confirmation' => 'pppppp',
            'budget' => 0
        ];
        $response = $this->json('POST', $this->url, $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('users', [
            'id' => 4,
            'name' => 'あ',
            'email' => 'test10@test.test',
            'budget' => 0
        ]);
        $token = $response->json()['data']['token'];
        $hash = hash('sha256', $token);
        $this->assertEquals($hash, User::find(4)->api_token);
        self::$token4 = $token;

        $data = [
            'name' => str_repeat('あ', 50),
            'email' => str_repeat('a', 245) . '@test.test',
            'password' => str_repeat('p', 128),
            'password_confirmation' => str_repeat('p', 128),
            'budget' => 1000000000
        ];
        $response = $this->json('POST', $this->url, $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('users', [
            'id' => 5,
            'name' => str_repeat('あ', 50),
            'email' => str_repeat('a', 245) . '@test.test',
            'budget' => 1000000000
        ]);
        $token = $response->json()['data']['token'];
        $hash = hash('sha256', $token);
        $this->assertEquals($hash, User::find(5)->api_token);
        self::$token5 = $token;
    }

    public function testShow()
    {
        $response = $this->json('GET', $this->url, [], ['Authorization' => 'Bearer ' . self::$token4]);
        $response->assertStatus(200)
            ->assertJson(['data' => ['name' => 'あ', 'email' => 'test10@test.test', 'budget' => 0]]);

        $this->refreshApplication();

        $response = $this->json('GET', $this->url, [], ['Authorization' => 'Bearer ' . self::$token5]);
        $response->assertStatus(200)
            ->assertJson(['data' => ['name' => str_repeat('あ', 50), 'email' => str_repeat('a', 245) . '@test.test', 'budget' => 1000000000]]);
    }

    /**
     * @dataProvider provideData
     * @dataProvider provideDataUpdate
     */
    public function testUpdateFail($data, $expect)
    {
        $response = $this->json('PATCH', $this->url, $data, ['Authorization' => 'Bearer ' . self::$token4]);
        $response->assertStatus(422)
            ->assertJson($expect);
        $this->assertDatabaseHas('users', [
            'id' => 4,
            'name' => 'あ',
            'email' => 'test10@test.test',
            'budget' => 0
        ]);
    }

    public function testUpdateSuccess()
    {
        $data = [
            'name' => 'ああ',
            'email' => 'test11@test.test',
            'password' => 'pppppppppp',
            'password_confirmation' => 'pppppppppp',
            'budget' => 100
        ];
        $response = $this->json('PATCH', $this->url, $data, ['Authorization' => 'Bearer ' . self::$token4]);
        $response->assertStatus(200);
        $this->assertDatabaseHas('users', [
            'id' => 4,
            'name' => 'ああ',
            'email' => 'test11@test.test',
            'budget' => 100
        ]);
        $token = $response->json()['data']['token'];
        $hash = hash('sha256', $token);
        $this->assertEquals($hash, User::find(4)->api_token);
        self::$token4 = $token;

        $data = [
            'name' => 'あああ',
            'email' => 'test12@test.test',
            'budget' => 1000
        ];
        $response = $this->json('PATCH', $this->url, $data, ['Authorization' => 'Bearer ' . self::$token4]);
        $response->assertStatus(200);
        $this->assertDatabaseHas('users', [
            'id' => 4,
            'name' => 'あああ',
            'email' => 'test12@test.test',
            'budget' => 1000
        ]);
        $response->assertExactJson([]);
    }

    public function testDestroy()
    {
        $response = $this->json('DELETE', $this->url, [], ['Authorization' => 'Bearer ' . self::$token4]);
        $response->assertStatus(200)
            ->assertExactJson([]);
        $this->assertDatabaseMissing('users', ['id' => 4]);
    }

    public function testAuth()
    {
        $response = $this->json('GET', $this->url);
        $response->assertStatus(401)->assertJson(['message' => 'Unauthenticated.']);

        $response = $this->json('PATCH', $this->url);
        $response->assertStatus(401)->assertJson(['message' => 'Unauthenticated.']);

        $response = $this->json('DELETE', $this->url);
        $response->assertStatus(401)->assertJson(['message' => 'Unauthenticated.']);
    }

    public function provideDataCreate()
    {
        return [[
            ['name' => '', 'email' => '', 'password' => '', 'budget' => ''],
            ['message' => 'The given data was invalid.', 'errors' => [
                'name' => ['nameは必ず指定してください。'],
                'email' => ['emailは必ず指定してください。'],
                'password' => ['passwordは必ず指定してください。'],
                'budget' => ['budgetは必ず指定してください。']
            ]]
        ]];
    }

    public function provideDataUpdate()
    {
        return [[
            ['name' => '', 'email' => '', 'password' => '', 'budget' => ''],
            ['message' => 'The given data was invalid.', 'errors' => [
                'name' => ['nameは必ず指定してください。'],
                'email' => ['emailは必ず指定してください。'],
                'budget' => ['budgetは必ず指定してください。']
            ]]
        ]];
    }

    public function provideData()
    {
        return [
            [
                ['name' => str_repeat('a', 51)],
                ['errors' => ['name' => ['nameは、50文字以下で指定してください。']]]
            ],
            [
                ['email' => 'a'],
                ['errors' => ['email' => ['emailには、有効なメールアドレスを指定してください。']]]
            ],
            [
                ['email' =>  'test1@test.test'],
                ['errors' => ['email' => ['emailの値は既に存在しています。']]]
            ],
            [
                ['email' => str_repeat('a', 246) . '@test.test'],
                ['errors' => ['email' => ['emailは、255文字以下で指定してください。']]]
            ],
            [
                ['password' => 'password'],
                ['errors' => ['password' => ['passwordと、確認フィールドとが、一致していません。']]]
            ],
            [
                ['password' => 'あ', 'password_confirmation' => 'あ'],
                ['errors' => ['password' => ['passwordはアルファベット数字がご利用できます。']]]
            ],
            [
                ['password' => str_repeat('a', 5), 'password_confirmation' => str_repeat('a', 5)],
                ['errors' => ['password' => ['passwordは、6文字以上で指定してください。']]]
            ],
            [
                ['password' => str_repeat('a', 129), 'password_confirmation' => str_repeat('a', 129)],
                ['errors' => ['password' => ['passwordは、128文字以下で指定してください。']]]
            ],
            [
                ['budget' => 'a'],
                ['errors' => ['budget' => ['budgetは整数で指定してください。']]]
            ],
            [
                ['budget' => '3.14'],
                ['errors' => ['budget' => ['budgetは整数で指定してください。']]]
            ],
            [
                ['budget' => '-1'],
                ['errors' => ['budget' => ['budgetには、0以上の数字を指定してください。']]]
            ],
            [
                ['budget' => '1000000001'],
                ['errors' => ['budget' => ['budgetには、1000000000以下の数字を指定してください。']]]
            ]
        ];
    }
}
