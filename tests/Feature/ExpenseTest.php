<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ExpenseTest extends TestCase
{
    protected $url = '/api/expenses';

    private static $token = null;

    public function setUp(): void
    {
        parent::setUp();
        if (config('app.env') == 'testing' && self::$token === null) {
            $data = ['email' => 'test1@test.test', 'password' => 'password'];
            $response = $this->json('POST', '/api/login', $data);
            self::$token = $response->json()['data']['token'];
        }
    }

    public function testIndex()
    {
        $response = $this->json('GET', $this->url . '?period=2019-12', [], ['Authorization' => 'Bearer ' . self::$token]);
        $response->assertStatus(200)
            ->assertJson([
                'data' => [['id' => 1, 'accounted_at' => '2019-12-01', 'title_id' => 15, 'title_name' => '給与', 'income' => 20000, 'outcome' => 0, 'note' => 'note1']],
                'meta' => ['transfer' => 84970, 'income' => 20000, 'outcome' => 34320]
            ]);
        $response->assertJsonCount(13, 'data');
    }

    /**
     * @dataProvider provideData
     * @dataProvider provideDataStore
     */
    public function testStoreFail($data, $expect)
    {
        $response = $this->json('POST', $this->url, $data, ['Authorization' => 'Bearer ' . self::$token]);
        $response->assertStatus(422)
            ->assertJson($expect);
        $this->assertDatabaseMissing('expenses', ['id' => 19]);
    }

    public function testStoreSuccess()
    {
        $data = ['type' => 'outcome', 'title_id' => 1, 'accounted_at' => '2020-01-10', 'amount' => 1, 'note' => 'n'];
        $response = $this->json('POST', $this->url, $data, ['Authorization' => 'Bearer ' . self::$token]);
        $response->assertStatus(200)
            ->assertExactJson([]);
        $this->assertDatabaseHas('expenses', [
            'id' => 19,
            'user_id' => 1,
            'title_id' => 1,
            'accounted_at' => '2020-01-10',
            'income' => 0,
            'outcome' => 1,
            'note' => 'n'
        ]);
        $this->assertDatabaseHas('expense_summaries', [
            'user_id' => 1,
            'period' => 202001,
            'income' => 0,
            'outcome' => 1
        ]);

        $data = ['type' => 'income', 'title_id' => 15, 'accounted_at' => '2020-01-11', 'amount' => 1000000000, 'note' => str_repeat('n', 255)];
        $response = $this->json('POST', $this->url, $data, ['Authorization' => 'Bearer ' . self::$token]);
        $response->assertStatus(200)
            ->assertExactJson([]);
        $this->assertDatabaseHas('expenses', [
            'id' => 20,
            'user_id' => 1,
            'title_id' => 15,
            'accounted_at' => '2020-01-11',
            'income' => 1000000000,
            'outcome' => 0,
            'note' => str_repeat('n', 255)
        ]);
        $this->assertDatabaseHas('expense_summaries', [
            'user_id' => 1,
            'period' => 202001,
            'income' => 1000000000,
            'outcome' => 1
        ]);
    }

    /**
     * @dataProvider provideData
     */
    public function testUpdateFail($data, $expect)
    {
        $response = $this->json('PATCH', $this->url . '/20', $data, ['Authorization' => 'Bearer ' . self::$token]);
        $response->assertStatus(422)
            ->assertJson($expect);
        $this->assertDatabaseHas('expenses', [
            'id' => 20,
            'user_id' => 1,
            'title_id' => 15,
            'accounted_at' => '2020-01-11',
            'income' => 1000000000,
            'outcome' => 0,
            'note' => str_repeat('n', 255)
        ]);
    }

    public function testUpdateSuccess()
    {
        $data = ['type' => 'income', 'title_id' => 16, 'accounted_at' => '2020-01-15', 'amount' => 1, 'note' => 'a'];
        $response = $this->json('PATCH', $this->url . '/20', $data, ['Authorization' => 'Bearer ' . self::$token]);
        $response->assertStatus(200)
            ->assertExactJson(['data' => ['income' => 1, 'outcome' => 1]]);
        $this->assertDatabaseHas('expenses', [
            'id' => 20,
            'user_id' => 1,
            'title_id' => 16,
            'accounted_at' => '2020-01-15',
            'income' => 1,
            'outcome' => 0,
            'note' => 'a'
        ]);
        $this->assertDatabaseHas('expense_summaries', [
            'user_id' => 1,
            'period' => 202001,
            'income' => 1,
            'outcome' => 1
        ]);

        $data = ['type' => 'income', 'title_id' => 16, 'accounted_at' => '2019-01-15', 'amount' => 1000000000, 'note' => str_repeat('n', 255)];
        $response = $this->json('PATCH', $this->url . '/20', $data, ['Authorization' => 'Bearer ' . self::$token]);
        $response->assertStatus(200)
            ->assertExactJson(['data' => ['income' => 0, 'outcome' => 1]]);
        $this->assertDatabaseHas('expenses', [
            'id' => 20,
            'user_id' => 1,
            'title_id' => 16,
            'accounted_at' => '2019-01-15',
            'income' => 1000000000,
            'outcome' => 0,
            'note' => str_repeat('n', 255)
        ]);
        $this->assertDatabaseHas('expense_summaries', [
            'user_id' => 1,
            'period' => 202001,
            'income' => 0,
            'outcome' => 1
        ]);
        $this->assertDatabaseHas('expense_summaries', [
            'user_id' => 1,
            'period' => 201901,
            'income' => 1000000000,
            'outcome' => 0
        ]);
    }

    public function testDestroy()
    {
        $response = $this->json('DELETE', $this->url . '/20', [], ['Authorization' => 'Bearer ' . self::$token]);
        $response->assertStatus(200)
            ->assertExactJson(['data' => ['income' => 0, 'outcome' => 0]]);
        $this->assertDatabaseMissing('expenses', ['id' => 20, 'deleted_at' => null]);
        $this->assertDatabaseHas('expense_summaries', [
            'user_id' => 1,
            'period' => 202001,
            'income' => 0,
            'outcome' => 1
        ]);
    }

    public function testAuth()
    {
        $response = $this->json('GET', $this->url);
        $response->assertStatus(401)
            ->assertJson(['message' => 'Unauthenticated.']);

        $response = $this->json('POST', $this->url);
        $response->assertStatus(401)
            ->assertJson(['message' => 'Unauthenticated.']);

        $response = $this->json('PATCH', $this->url . '/1');
        $response->assertStatus(401)
            ->assertJson(['message' => 'Unauthenticated.']);

        $response = $this->json('DELETE', $this->url . '/1');
        $response->assertStatus(401)
            ->assertJson(['message' => 'Unauthenticated.']);

        $response = $this->json('PATCH', $this->url . '/18', [], ['Authorization' => 'Bearer ' . self::$token]);
        $response->assertStatus(403)
            ->assertJson(['message' => 'Forbidden.', 'errors' => ['Access' => ['そのリソースへのアクセスは禁止されています。']]]);

        $response = $this->json('DELETE', $this->url . '/18', [], ['Authorization' => 'Bearer ' . self::$token]);
        $response->assertStatus(403)
            ->assertJson(['message' => 'Forbidden.', 'errors' => ['Access' => ['そのリソースへのアクセスは禁止されています。']]]);
    }

    public function provideDataStore()
    {
        return [
            [
                ['type' => '', 'title_id' => '', 'accounted_at' => '', 'amount' => ''],
                ['message' => 'The given data was invalid.', 'errors' => [
                    'type' => ['typeは必ず指定してください。'],
                    'title_id' => ['title idは必ず指定してください。'],
                    'accounted_at' => ['accounted atは必ず指定してください。'],
                    'amount' => ['amountは必ず指定してください。']
                ]]
            ],
            [
                ['type' => 'aaa'],
                ['errors' => ['type' => ['選択されたtypeは正しくありません。']]]
            ],
            [
                ['type' => 'income', 'title_id' => 1, 'accounted_at' => '2019-12-30', 'amount' => 100],
                ['errors' => ['title_id' => ['その費目は指定出来ません。']]]
            ]
        ];
    }

    public function provideData()
    {
        return [
            [
                ['type' => '', 'title_id' => '', 'accounted_at' => '', 'amount' => ''],
                ['message' => 'The given data was invalid.', 'errors' => [
                    'title_id' => ['title idは必ず指定してください。'],
                    'accounted_at' => ['accounted atは必ず指定してください。'],
                    'amount' => ['amountは必ず指定してください。']
                ]]
            ],
            [
                ['title_id' => 100],
                ['errors' => ['title_id' => ['選択されたtitle idは正しくありません。']]]
            ],
            [
                ['type' => 'income', 'title_id' => 20, 'accounted_at' => '2020-01-10', 'amount' => 100],
                ['errors' => ['title_id' => ['その費目は指定出来ません。']]]
            ],
            [
                ['type' => 'income', 'title_id' => 1, 'accounted_at' => '2020-01-10', 'amount' => 100],
                ['errors' => ['title_id' => ['その費目は指定出来ません。']]]
            ],
            [
                ['accounted_at' => '2020-01-40'],
                ['errors' => ['accounted_at' => ['accounted atには有効な日付を指定してください。']]]
            ],
            [
                ['amount' => 'a'],
                ['errors' => ['amount' => ['amountは整数で指定してください。']]]
            ],
            [
                ['amount' => 0],
                ['errors' => ['amount' => ['amountには、1以上の数字を指定してください。']]]
            ],
            [
                ['amount' => 1000000001],
                ['errors' => ['amount' => ['amountには、1000000000以下の数字を指定してください。']]]
            ],
            [
                ['note' => str_repeat('a', 256)],
                ['errors' => ['note' => ['noteは、255文字以下で指定してください。']]]
            ],
        ];
    }
}
