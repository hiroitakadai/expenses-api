<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TitleTest extends TestCase
{
    protected $url = '/api/titles';

    private static $token = null;

    public function setUp(): void
    {
        parent::setUp();
        if (config('app.env') == 'testing' && self::$token === null) {
            $data = ['email' => 'test1@test.test', 'password' => 'password'];
            $response = $this->json('POST', '/api/login', $data);
            self::$token = $response->json()['data']['token'];
        }
    }

    public function testIndex()
    {
        $response = $this->json('GET', $this->url, [], ['Authorization' => 'Bearer ' . self::$token]);
        $response->assertStatus(200)
            ->assertJson(['data' => [['value' => 1, 'text' => '食費', 'is_income' => false, 'is_outcome' => true]]]);
        $response->assertJsonCount(18, 'data');
    }

    /**
     * @dataProvider provideDataName
     * @dataProvider provideDataType
     */
    public function testStoreFail($data, $expect)
    {
        $response = $this->json('POST', $this->url, $data, ['Authorization' => 'Bearer ' . self::$token]);
        $response->assertStatus(422)
            ->assertJson($expect);
        $this->assertDatabaseMissing('titles', ['id' => 55]);
    }

    public function testStoreSuccess()
    {
        $data = ['name' => 'あ', 'type' => 'outcome'];
        $response = $this->json('POST', $this->url, $data, ['Authorization' => 'Bearer ' . self::$token]);
        $response->assertStatus(200)
            ->assertJson(['data' => ['id' => 55]]);
        $this->assertDatabaseHas('titles', [
            'id' => 55,
            'name' => 'あ',
            'is_income' => false,
            'is_outcome' => true,
            'user_id' => 1
        ]);

        $data = ['name' => str_repeat('あ', 10), 'type' => 'outcome'];
        $response = $this->json('POST', $this->url, $data, ['Authorization' => 'Bearer ' . self::$token]);
        $response->assertStatus(200)
            ->assertJson(['data' => ['id' => 56]]);
        $this->assertDatabaseHas('titles', [
            'id' => 56,
            'name' => str_repeat('あ', 10),
            'is_income' => false,
            'is_outcome' => true,
            'user_id' => 1
        ]);
    }

    /**
     * @dataProvider provideDataName
     */
    public function testUpdateFail($data, $expect)
    {
        $response = $this->json('PATCH', $this->url . '/55', $data, ['Authorization' => 'Bearer ' . self::$token]);
        $response->assertStatus(422)
            ->assertJson($expect);
        $this->assertDatabaseHas('titles', [
            'id' => 55,
            'name' => 'あ',
            'is_income' => false,
            'is_outcome' => true,
            'user_id' => 1
        ]);
    }

    public function testUpdateSuccess()
    {
        $data = ['name' => 'あ'];
        $response = $this->json('PATCH', $this->url . '/55', $data, ['Authorization' => 'Bearer ' . self::$token]);
        $response->assertStatus(200)
            ->assertExactJson([]);
        $this->assertDatabaseHas('titles', [
            'id' => 55,
            'name' => 'あ',
            'is_income' => false,
            'is_outcome' => true,
            'user_id' => 1
        ]);

        $data = ['name' => str_repeat('あ', 10)];
        $response = $this->json('PATCH', $this->url . '/55', $data, ['Authorization' => 'Bearer ' . self::$token]);
        $response->assertStatus(200)
            ->assertExactJson([]);
        $this->assertDatabaseHas('titles', [
            'id' => 55,
            'name' => str_repeat('あ', 10),
            'is_income' => false,
            'is_outcome' => true,
            'user_id' => 1
        ]);
    }

    public function testDestroy()
    {
        $response = $this->json('DELETE', $this->url . '/55', [], ['Authorization' => 'Bearer ' . self::$token]);
        $response->assertStatus(200)
            ->assertExactJson([]);
        $this->assertDatabaseMissing('titles', ['id' => 55]);
    }

    public function testAuth()
    {
        $response = $this->json('GET', $this->url);
        $response->assertStatus(401)
            ->assertJson(['message' => 'Unauthenticated.']);

        $response = $this->json('POST', $this->url);
        $response->assertStatus(401)
            ->assertJson(['message' => 'Unauthenticated.']);

        $response = $this->json('PATCH', $this->url . '/1');
        $response->assertStatus(401)
            ->assertJson(['message' => 'Unauthenticated.']);

        $response = $this->json('DELETE', $this->url . '/1');
        $response->assertStatus(401)
            ->assertJson(['message' => 'Unauthenticated.']);

        $response = $this->json('PATCH', $this->url . '/19', [], ['Authorization' => 'Bearer ' . self::$token]);
        $response->assertStatus(403)
            ->assertJson(['message' => 'Forbidden.', 'errors' => ['Access' => ['そのリソースへのアクセスは禁止されています。']]]);

        $response = $this->json('DELETE', $this->url . '/19', [], ['Authorization' => 'Bearer ' . self::$token]);
        $response->assertStatus(403)
            ->assertJson(['message' => 'Forbidden.', 'errors' => ['Access' => ['そのリソースへのアクセスは禁止されています。']]]);
    }

    public function provideDataType()
    {
        return [
            [
                ['type' => ''],
                ['errors' => ['type' => ['typeは必ず指定してください。']]]
            ],
            [
                ['type' => 'aaa'],
                ['errors' => ['type' => ['選択されたtypeは正しくありません。']]]
            ]
        ];
    }

    public function provideDataName()
    {
        return [
            [
                ['name' => ''],
                ['errors' => ['name' => ['nameは必ず指定してください。']]]
            ],
            [
                ['name' => str_repeat('a', 11)],
                ['errors' => ['name' => ['nameは、10文字以下で指定してください。']]]
            ]
        ];
    }
}
