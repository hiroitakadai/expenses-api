<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoginTest extends TestCase
{
    protected $login = '/api/login';
    protected $logout = '/api/logout';

    private static $token = null;

    public function testLogin()
    {
        $data = [];
        $response = $this->json('POST', $this->login, $data);
        $response->assertStatus(401)
            ->assertJson(['message' => 'Unauthorized.', 'errors' => ['Auth' => ['メールアドレスとパスワードの組み合わせが正しくありません。']]]);

        $data = ['email' => 'test1@test.test', 'password' => 'p'];
        $response = $this->json('POST', $this->login, $data);
        $response->assertStatus(401)
            ->assertJson(['message' => 'Unauthorized.', 'errors' => ['Auth' => ['メールアドレスとパスワードの組み合わせが正しくありません。']]]);

        $user = User::find(1);
        $data = ['email' => 'test1@test.test', 'password' => 'password'];
        $response = $this->json('POST', $this->login, $data);
        $response->assertStatus(200);
        $token = $response->json()['data']['token'];
        $hash = hash('sha256', $token);
        $this->assertEquals($hash, User::find(1)->api_token);
        $this->assertNotEquals($user->api_token, User::find(1)->api_token);
        self::$token = $token;
    }

    public function testLogout()
    {
        $response = $this->json('DELETE', $this->logout);
        $response->assertStatus(401)
            ->assertJson(['message' => 'Unauthenticated.']);

        $response = $this->json('DELETE', $this->logout, [], ['Authorization' => 'Bearer ' . self::$token]);
        $response->assertStatus(200)->assertExactJson([]);
        $this->assertNull(User::find(1)->api_token);
    }
}
