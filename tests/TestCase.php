<?php

namespace Tests;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    private static $isSetup = false;

    public function setUp(): void
    {
        parent::setUp();

        if (self::$isSetup === false) {
            Artisan::call('cache:clear');
            Artisan::call('config:clear');
            Artisan::call('optimize:clear');
            Artisan::call('route:clear');
        }

        if (App::environment('testing') && self::$isSetup === false) {
            Artisan::call('migrate:refresh');
            Artisan::call('db:seed');
            self::$isSetup = true;
        }
    }
}
