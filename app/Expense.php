<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Expense extends Model
{
    use SoftDeletes;

    protected $fillable = ['accounted_at', 'title_id', 'income', 'outcome', 'note'];

    function saveExpense($request)
    {
        $this->title_id = $request->input('title_id');
        $this->accounted_at = $request->input('accounted_at');
        $this->note = $request->input('note');
        $this->user_id = Auth::id();

        if ($this->income > 0 || $request->input('type') === 'income') {
            $this->income = $request->input('amount');
        } else {
            $this->income = 0;
        }

        if ($this->outcome > 0 || $request->input('type') === 'outcome') {
            $this->outcome = $request->input('amount');
        } else {
            $this->outcome = 0;
        }

        return $this->save();
    }
}
