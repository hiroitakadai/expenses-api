<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Title extends Model
{

    static function initData($user_id)
    {
        DB::table('titles')->insert([
            ['user_id' => $user_id, 'name' => '食費',     'is_income' => false, 'is_outcome' => true],
            ['user_id' => $user_id, 'name' => '雑貨',     'is_income' => false, 'is_outcome' => true],
            ['user_id' => $user_id, 'name' => '交通',     'is_income' => false, 'is_outcome' => true],
            ['user_id' => $user_id, 'name' => '交際',     'is_income' => false, 'is_outcome' => true],
            ['user_id' => $user_id, 'name' => '趣味',     'is_income' => false, 'is_outcome' => true],
            ['user_id' => $user_id, 'name' => '教育',     'is_income' => false, 'is_outcome' => true],
            ['user_id' => $user_id, 'name' => '衣服',     'is_income' => false, 'is_outcome' => true],
            ['user_id' => $user_id, 'name' => '医療',     'is_income' => false, 'is_outcome' => true],
            ['user_id' => $user_id, 'name' => '通信',     'is_income' => false, 'is_outcome' => true],
            ['user_id' => $user_id, 'name' => '光熱',     'is_income' => false, 'is_outcome' => true],
            ['user_id' => $user_id, 'name' => '住まい',   'is_income' => false, 'is_outcome' => true],
            ['user_id' => $user_id, 'name' => 'クルマ',   'is_income' => false, 'is_outcome' => true],
            ['user_id' => $user_id, 'name' => '税金',     'is_income' => false, 'is_outcome' => true],
            ['user_id' => $user_id, 'name' => 'その他',   'is_income' => false, 'is_outcome' => true],
            ['user_id' => $user_id, 'name' => '給与',     'is_income' => true,  'is_outcome' => false],
            ['user_id' => $user_id, 'name' => '臨時収入', 'is_income' => true,  'is_outcome' => false],
            ['user_id' => $user_id, 'name' => '事業収入', 'is_income' => true,  'is_outcome' => false],
            ['user_id' => $user_id, 'name' => 'その他',   'is_income' => true,  'is_outcome' => false],
        ]);
    }
}
