<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ExpenseSummary extends Model
{
    protected $fillable = array('period', 'income', 'outcome', 'user_id');


    static function aggregate($accounted_at, $user_id = null)
    {
        if ($user_id === null) {
            $user_id = Auth::id();
        }

        $period = new Carbon($accounted_at);

        $aggregate = Expense::select(DB::raw("SUM(income) AS income, SUM(outcome) AS outcome"))
            ->where('user_id', $user_id)
            ->where('accounted_at', 'like', $period->format('Y-m%'))
            ->first();

        $income = 0;
        $outcome = 0;
        if ($aggregate && $aggregate->income !== null) {
            $income = $aggregate->income;
        }

        if ($aggregate && $aggregate->outcome !== null) {
            $outcome = $aggregate->outcome;
        }

        return ExpenseSummary::updateOrCreate(
            ['user_id' => $user_id, 'period' => $period->format('Ym')],
            ['income' => $income, 'outcome' => $outcome]
        );
    }
}
