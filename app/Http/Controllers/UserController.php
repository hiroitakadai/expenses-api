<?php

namespace App\Http\Controllers;

use App\Title;
use App\User;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:50',
            'email' => 'required|email|unique:users,email|max:255',
            'password' => 'required|regex:/^[a-z0-9]+$/i|confirmed|min:6|max:128',
            'budget' => 'required|integer|min:0|max:1000000000'
        ]);

        $token = Str::random(60);

        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'budget' => $request->input('budget'),
            'api_token' => hash('sha256', $token),
        ]);

        Title::initData($user->id);

        return ['data' => ['token' => $token]];
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return UserResource::make(Auth::user());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:50',
            'email' => ['required', 'email', 'max:255', Rule::unique('users')->ignore(Auth::id())],
            'password' => 'required|string',
            'new_password' => 'nullable|regex:/^[a-z0-9]+$/i|confirmed|min:6|max:128',
            'budget' => 'required|integer|min:0|max:1000000000'
        ]);

        if (!Hash::check($request->input('password'), Auth::user()->password)) {
            $res = ["message" => "Unprocessable Entity.", "errors" => ["Password" => ["パスワードが正しくありません。"]]];
            return response()->json($res, 422);
        }

        $user = User::find(Auth::id());
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->budget = $request->input('budget');
        if ($request->input('new_password')) {
            $token = Str::random(60);
            $user->password = Hash::make($request->input('new_password'));
            $user->api_token = hash('sha256', $token);
        }

        $user->save();

        if ($request->input('new_password')) {
            return ['data' => ['token' => $token]];
        }

        return [];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        User::find(Auth::id())->delete();

        return [];
    }

    /**
     * Login
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $api_token = Str::random(60);

            $user = User::find(Auth::id());
            $user->api_token = hash('sha256', $api_token);
            $user->save();

            return ['data' => ['token' => $api_token]];
        }

        $res = ["message" => "Unauthorized.", "errors" => ["Auth" => ["メールアドレスとパスワードの組み合わせが正しくありません。"]]];
        return response()->json($res, 401);
    }

    /**
     * Logout
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        $user = User::find(Auth::id());
        $user->api_token = null;
        $user->save();

        return [];
    }
}
