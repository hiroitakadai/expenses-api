<?php

namespace App\Http\Controllers;

use App\Title;
use App\Http\Resources\TitleResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class TitleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $res = TitleResource::collection(
            Title::where('user_id', Auth::id())->get()
        );
        return ['data' => $res];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'type' => ['required', Rule::in(['income', 'outcome'])],
            'name' => 'required|max:10'
        ]);

        $title = new Title();
        $title->name = $request->input('name');
        $title->user_id = Auth::id();

        switch ($request->input('type')) {
            case "income":
                $title->is_income = true;
                break;
            case "outcome":
                $title->is_outcome = true;
                break;
        }

        $title->save();

        return ['data' => ['id' => $title->id]];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Title  $title
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Title $title)
    {
        if ($title->user_id !== Auth::id()) {
            $res = ["message" => "Forbidden.", "errors" => ["Access" => ["そのリソースへのアクセスは禁止されています。"]]];
            return response()->json($res, 403);
        }

        $request->validate([
            'name' => 'required|max:10'
        ]);

        $title->name = $request->input('name');
        $title->save();

        return [];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Title  $title
     * @return \Illuminate\Http\Response
     */
    public function destroy(Title $title)
    {
        if ($title->user_id !== Auth::id()) {
            $res = ["message" => "Forbidden.", "errors" => ["Access" => ["そのリソースへのアクセスは禁止されています。"]]];
            return response()->json($res, 403);
        }

        $title->delete();

        return [];
    }
}
