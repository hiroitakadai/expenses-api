<?php

namespace App\Http\Controllers;

use App\Expense;
use App\ExpenseSummary;
use App\Title;
use App\Http\Resources\ExpenseResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the expense.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $period = (!empty($request->input('period'))) ? $request->input('period') : date('Y-m');

        $res['data'] = ExpenseResource::collection(
            Expense::where('user_id', Auth::id())
                ->where('accounted_at', 'like', $period . '%')
                ->orderBy('accounted_at')
                ->get()
        );

        $total = ExpenseSummary::select(DB::raw("SUM(income) AS income, SUM(outcome) AS outcome"))
            ->where('user_id', Auth::id())
            ->where('period', '<', str_replace('-', '', $period))
            ->first();

        if ($total) {
            $res['meta']['transfer'] = $total->income - $total->outcome;
        } else {
            $res['meta']['transfer'] = 0;
        }

        $summary = ExpenseSummary::where('user_id', Auth::id())
            ->where('period', str_replace('-', '', $period))
            ->first();

        if ($summary) {
            $res['meta']['income'] = $summary->income;
            $res['meta']['outcome'] = $summary->outcome;
        } else {
            $res['meta']['income'] = 0;
            $res['meta']['outcome'] = 0;
        }

        return $res;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'type' => ['required', Rule::in(['income', 'outcome'])],
            'title_id' => 'required|exists:titles,id',
            'accounted_at' => 'required|date',
            'amount' => 'required|integer|min:1|max:1000000000',
            'note' => 'max:255'
        ]);

        if (Title::where('id', $request->input('title_id'))->where('user_id', Auth::id())->where('is_' . $request->input('type'), true)->first() === null) {
            $res = ["message" => "The given data was invalid.", "errors" => ["title_id" => ["その費目は指定出来ません。"]]];
            return response()->json($res, 422);
        }

        $expense = new Expense();
        $expense->saveExpense($request);

        ExpenseSummary::aggregate($request->input('accounted_at'));

        return [];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Expense $expense)
    {
        if ($expense->user_id !== Auth::id()) {
            $res = ["message" => "Forbidden.", "errors" => ["Access" => ["そのリソースへのアクセスは禁止されています。"]]];
            return response()->json($res, 403);
        }

        $request->validate([
            'title_id' => 'required|exists:titles,id',
            'accounted_at' => 'required|date',
            'amount' => 'required|integer|min:1|max:1000000000',
            'note' => 'max:255'
        ]);

        $title = Title::where('id', $request->input('title_id'))->where('user_id', Auth::id())->first();

        if ($title === null) {
            $res = ["message" => "The given data was invalid.", "errors" => ["title_id" => ["その費目は指定出来ません。"]]];
            return response()->json($res, 422);
        }

        if ($title->is_income === true && $expense->income === 0) {
            $res = ["message" => "The given data was invalid.", "errors" => ["title_id" => ["その費目は指定出来ません。"]]];
            return response()->json($res, 422);
        }

        if ($title->is_outcome === true && $expense->outcome === 0) {
            $res = ["message" => "The given data was invalid.", "errors" => ["title_id" => ["その費目は指定出来ません。"]]];
            return response()->json($res, 422);
        }

        $old_date = $expense->accounted_at;

        $expense->saveExpense($request);

        $summary = ExpenseSummary::aggregate($request->input('accounted_at'));
        $summary = ExpenseSummary::aggregate($old_date);

        return ["data" => ["income" => $summary->income, "outcome" => $summary->outcome]];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function destroy(Expense $expense)
    {
        if ($expense->user_id !== Auth::id()) {
            $res = ["message" => "Forbidden.", "errors" => ["Access" => ["そのリソースへのアクセスは禁止されています。"]]];
            return response()->json($res, 403);
        }

        $expense->delete();

        $summary = ExpenseSummary::aggregate($expense->accounted_at);

        return ['data' => ['income' => $summary->income, 'outcome' => $summary->outcome]];
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\Http\Response
     */
    public function period()
    {
        $res = Expense::select(DB::raw('TO_CHAR(accounted_at, \'YYYY-MM\') AS period, count(*) AS count'))
            ->where('user_id', Auth::id())
            ->groupBy('period')
            ->orderBy('period', 'desc')
            ->get();

        return ['data' => $res];
    }

    /**
     * Undocumented function
     *
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function summary(Request $request)
    {
        $period = (!empty($request->input('period'))) ? $request->input('period') : date('Y-m');

        $res['data'] = Expense::select(DB::raw("accounted_at, SUM(outcome) AS outcome"))
            ->where('user_id', Auth::id())
            ->where('outcome', '>', 0)
            ->where('accounted_at', 'like', $period . '%')
            ->groupBy('accounted_at')
            ->get();

        $res['meta']['budget'] = $balance = Auth::user()->budget;
        foreach ($res['data'] as &$expense) {
            $balance -= $expense['outcome'];
            $expense['balance'] = $balance;
        }

        $summary = ExpenseSummary::where('user_id', Auth::id())
            ->where('period', str_replace('-', '', $period))
            ->first();

        if ($summary) {
            $res['meta']['income'] = $summary->income;
            $res['meta']['outcome'] = $summary->outcome;
        } else {
            $res['meta']['income'] = 0;
            $res['meta']['outcome'] = 0;
        }

        return $res;
    }
}
