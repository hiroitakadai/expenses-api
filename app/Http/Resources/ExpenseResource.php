<?php

namespace App\Http\Resources;

use App\Title;
use Illuminate\Http\Resources\Json\JsonResource;

class ExpenseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'accounted_at' => $this->accounted_at,
            'title_id' => $this->title_id,
            'title_name' => (Title::find($this->title_id)) ? Title::find($this->title_id)->name : "",
            'income' => $this->income,
            'outcome' => $this->outcome,
            'note' => $this->note,
        ];
    }
}
